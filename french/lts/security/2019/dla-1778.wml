#use wml::debian::translation-check translation="0489e1b952f47155cfd52286f4b52319011dbc06" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans symfony, un
cadriciel PHP pour applications web. De nombreux composants de symfony sont
affectés : Framework Bundle, Dependency Injection, Security, HttpFoundation</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10909">CVE-2019-10909</a>

<p>Les messages de validation n’étaient pas protégés lors de l’utilisation du
thème de formulaire du moteur de modèles de PHP. Lorsque les messages de
validation pouvaient contenir une entrée de l’utilisateur, cela pouvait aboutir
à un script intersite (XSS).</p>

<p>Pour plus d’informations, veuillez consulter l’annonce de l’amont à
<a href="https://symfony.com/blog/cve-2019-10909-escape-validation-messages-in-the-php-templating-engine">https://symfony.com/blog/cve-2019-10909-escape-validation-messages-in-the-php-templating-engine</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10910">CVE-2019-10910</a>

<p>Les ID de service déduits d’une entrée utilisateur non filtrée pourraient
aboutir à l’exécution de n’importe quel code arbitraire, conduisant à une
possible exécution de code à distance.</p>

<p>Pour plus d’informations, veuillez consulter l’annonce de l’amont à
<a href="https://symfony.com/blog/cve-2019-10910-check-service-ids-are-valid">https://symfony.com/blog/cve-2019-10910-check-service-ids-are-valid</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10911">CVE-2019-10911</a>

<p>Cela corrige des situations où une partie du temps d’expiration dans un cookie
pourrait être considérée comme une partie du nom d’utilisateur, ou une partie du
nom de l’utilisateur considérée comme une partie du temps d’expiration. Un
attaquant pourrait modifier le cookie « remember me » et s’authentifier comme
utilisateur différent. Cette attaque est seulement possible si la fonctionnalité
« remember me » est activée et que les deux utilisateurs partagent un hachage de
mot de passe ou que les hachages de mot de passe (par exemple,
UserInterface::getPassword()) soient Null pour tous les utilisateurs (ce qui est
valable si les mots de passe sont vérifiés par un système externe, par exemple,
un SSO).</p>

<p>Pour plus d’informations, veuillez consulter l’annonce de l’amont à
<a href="https://symfony.com/blog/cve-2019-10911-add-a-separator-in-the-remember-me-cookie-hash">https://symfony.com/blog/cve-2019-10911-add-a-separator-in-the-remember-me-cookie-hash</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10913">CVE-2019-10913</a>

<p>Les méthodes HTTP de la méthode HTTP elle-même ou en utilisant l’en-tête
X-Http-Method-Override, étaient précédemment renvoyées comme méthode concernée
sans validation réalisée sur la chaîne, signifiant qu’elles pourraient être
utilisées dans des contextes dangereux lorsque laissées non protégées.</p>

<p>Pour plus d’informations, veuillez consulter l’annonce de l’amont à
<a href="https://symfony.com/blog/cve-2019-10913-reject-invalid-http-method-overrides">https://symfony.com/blog/cve-2019-10913-reject-invalid-http-method-overrides</a></p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.3.21+dfsg-4+deb8u5.</p>
<p>Nous vous recommandons de mettre à jour vos paquets symfony.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>


</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1778.data"
# $Id: $
