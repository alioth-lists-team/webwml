#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il existait une vulnérabilité de script intersite (XSS) dans phpldapadmin,
une interface basée sur le web pour l’administration de serveurs LDAP.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la
version 1.2.2-5.2+deb8u1.
<p><b>Remarque</b> : le journal de modifications fait référence indûment à un
identifiant <a href="https://security-tracker.debian.org/tracker/CVE-2016-11107">CVE-2016-11107</a>
non existant. L’identifiant correct pour ce problème est
<a href="https://security-tracker.debian.org/tracker/CVE-2017-11107">CVE-2017-11107</a>.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpldapadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1561.data"
# $Id: $
