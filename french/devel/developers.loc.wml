#use wml::debian::template title="Emplacement des développeurs"
#use wml::debian::translation-check translation="6c996020eca890cac2a0243d4633d9bfa7935674" maintainer="Thomas Huriaux"
# Original translation by Denis Barbier

<p>Beaucoup de personnes ont manifesté de l'intérêt pour connaître
l'emplacement géographique des développeurs Debian.
Nous avons donc décidé d'ajouter dans la base de données des
développeurs un champ dans lequel ils peuvent spécifier leurs
coordonnées géographiques.

<p>La carte ci-dessous a été générée par le programme
<a href="https://packages.debian.org/stable/graphics/xplanet">xplanet</a>
à partir de cette <a href="developers.coords">liste des coordonnées
des développeurs</a>, en supprimant toute information personnelle.

<p><img src="developers.map.jpeg" alt="Planisphère">

<p>Si vous êtes un développeur et souhaitez voir vos coordonnées
ajoutées à la base de données, entrez dans la
<a href="https://db.debian.org">base de données des développeurs Debian</a>
et modifiez le champ correspondant. Si vous ne connaissez pas votre
position, vous pouvez la trouver à partir du lien suivant&nbsp;:
<ul>
<li><a href="https://osm.org">Openstreetmap</a> :
Vous pouvez rechercher votre ville en utilisant la case « Recherche ».
Sélectionnez les flèches de direction à côté de cette barre. Puis, faites
glisser le repère vert sur la carte d’OSM. Les coordonnées apparaissent dans la
case « De ».
</ul>

<p>Le format pour les coordonnées est un de ceux-ci&nbsp;:
<dl>
<dt>degrés décimaux
<dd>Le format est +-DDD.DDDDDDDDDDDDDDD. C'est le format utilisé par des
    programmes comme xearth ainsi que par de nombreux sites de
    positionnement. Cependant, la précision est souvent limitée à
    4 ou 5 décimales.
<dt>degrés minutes (DGM)
<dd>Le format est +-DDDMM.MMMMMMMMMMMMM. Ce n'est pas un type
    arithmétique, mais une représentation compacte de deux unités
    distinctes, les degrés et les minutes. Cette sortie est courante
    dans certains types d'appareils GPS portables.
<dt>degrés minutes secondes (DGMS)
<dd>Le format est +-DDDMMSS.SSSSSSSSSSS. Comme pour les DGM, ce n'est
    pas un type arithmétique, mais une représentation compacte de trois
    unités distinctes&nbsp;: les degrés, minutes et secondes. Cette
    sortie vient typiquement de sites web qui donnent trois valeurs pour
    chaque position. Par exemple, si la position fournie est
    34:50:12.24523 Nord, elle est +0345012.24523 en DGMS.
</dl>

<p>
Pour la latitude, + est le nord, pour la longitude, + est l'est. Il est
important de spécifier suffisamment de zéros au début de la position
pour être sûr de supprimer les ambiguïtés si la position est à moins de
2 degrés du point 0.

