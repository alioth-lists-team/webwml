#
# Damyan Ivanov <dmn@debian.org>, 2011, 2012, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.51\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2013-08-27 14:11+0200\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@fsa-bg.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.5.4\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "възлагателно писмо"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "възлагателно писмо"

#. One male delegate
#: ../../english/intro/organization.data:18
msgid "<void id=\"male\"/>delegate"
msgstr "делегат"

#. One female delegate
#: ../../english/intro/organization.data:20
msgid "<void id=\"female\"/>delegate"
msgstr "делегат"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:25
msgid "current"
msgstr "текущ"

#: ../../english/intro/organization.data:27
#: ../../english/intro/organization.data:29
msgid "member"
msgstr "член"

#: ../../english/intro/organization.data:32
msgid "manager"
msgstr "отговорник"

#: ../../english/intro/organization.data:34
msgid "Stable Release Manager"
msgstr "Управител на стабилното издание"

#: ../../english/intro/organization.data:34
msgid "SRM"
msgstr "УСИ"

#: ../../english/intro/organization.data:36
msgid "wizard"
msgstr "магьосник"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:38
#, fuzzy
msgid "chair"
msgstr "председател"

#: ../../english/intro/organization.data:41
msgid "assistant"
msgstr "асистент"

#: ../../english/intro/organization.data:43
msgid "secretary"
msgstr "секретар"

#: ../../english/intro/organization.data:45
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:47
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:63
#: ../../english/intro/organization.data:75
msgid "Officers"
msgstr "Официални длъжности"

#: ../../english/intro/organization.data:64
#: ../../english/intro/organization.data:99
msgid "Distribution"
msgstr "Дистрибуция"

#: ../../english/intro/organization.data:65
#: ../../english/intro/organization.data:235
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:67
#: ../../english/intro/organization.data:238
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:68
#: ../../english/intro/organization.data:242
#, fuzzy
msgid "Publicity team"
msgstr "Публичност"

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:311
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:339
msgid "Support and Infrastructure"
msgstr "Поддръжка и инфраструктура"

#: ../../english/intro/organization.data:78
msgid "Leader"
msgstr "Лидер"

#: ../../english/intro/organization.data:80
msgid "Technical Committee"
msgstr "Техническа комисия"

#: ../../english/intro/organization.data:94
msgid "Secretary"
msgstr "Секретар"

#: ../../english/intro/organization.data:102
msgid "Development Projects"
msgstr "Разработка"

#: ../../english/intro/organization.data:103
msgid "FTP Archives"
msgstr "FTP архиви"

#: ../../english/intro/organization.data:105
msgid "FTP Masters"
msgstr "Магьосник на FTP"

#: ../../english/intro/organization.data:111
msgid "FTP Assistants"
msgstr "Асистенти"

#: ../../english/intro/organization.data:116
msgid "FTP Wizards"
msgstr "Магьосници на FTP"

#: ../../english/intro/organization.data:120
msgid "Backports"
msgstr "Обновени пакети за стабилното издание"

#: ../../english/intro/organization.data:122
msgid "Backports Team"
msgstr "Екип за поддържане на обновените пакети за стабилното издание"

#: ../../english/intro/organization.data:126
msgid "Release Management"
msgstr "Управление на изданията"

#: ../../english/intro/organization.data:128
msgid "Release Team"
msgstr "Екип по изданията"

#: ../../english/intro/organization.data:141
msgid "Quality Assurance"
msgstr "Контрол на качеството"

#: ../../english/intro/organization.data:142
msgid "Installation System Team"
msgstr "Екип по инсталационната система"

#: ../../english/intro/organization.data:143
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Release Notes"
msgstr "Бележки по изданията"

#: ../../english/intro/organization.data:146
msgid "CD Images"
msgstr "Образи на компактдискове"

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr "Производство"

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr "Тестване"

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:162
msgid "Autobuilding infrastructure"
msgstr "Инфраструктура за автоматично компилиране на пакети"

#: ../../english/intro/organization.data:164
msgid "Wanna-build team"
msgstr "Екип на wanna-build"

#: ../../english/intro/organization.data:171
msgid "Buildd administration"
msgstr "Администриране на buildd"

#: ../../english/intro/organization.data:189
msgid "Documentation"
msgstr "Документация"

#: ../../english/intro/organization.data:194
msgid "Work-Needing and Prospective Packages list"
msgstr "Списък на незавършени и планирани пакети"

#: ../../english/intro/organization.data:196
msgid "Ports"
msgstr "Архитектури"

#: ../../english/intro/organization.data:226
msgid "Special Configurations"
msgstr "Специални конфигурации"

#: ../../english/intro/organization.data:228
msgid "Laptops"
msgstr "Преносими компютри"

#: ../../english/intro/organization.data:229
msgid "Firewalls"
msgstr "Защитни стени"

#: ../../english/intro/organization.data:230
msgid "Embedded systems"
msgstr "Системи за вграждане"

#: ../../english/intro/organization.data:245
msgid "Press Contact"
msgstr "Контакт с пресата"

#: ../../english/intro/organization.data:247
msgid "Web Pages"
msgstr "Уеб-страници"

#: ../../english/intro/organization.data:259
msgid "Planet Debian"
msgstr "Планета Дебиан"

#: ../../english/intro/organization.data:264
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:269
msgid "Debian Women Project"
msgstr "Проект Жени в Дебиан"

#: ../../english/intro/organization.data:277
msgid "Anti-harassment"
msgstr "Без тормоз"

#: ../../english/intro/organization.data:282
msgid "Events"
msgstr "Събития"

#: ../../english/intro/organization.data:289
#, fuzzy
msgid "DebConf Committee"
msgstr "Техническа комисия"

#: ../../english/intro/organization.data:296
msgid "Partner Program"
msgstr "Партньоркса програма"

#: ../../english/intro/organization.data:301
msgid "Hardware Donations Coordination"
msgstr "Координация на хардуерни дарения"

#: ../../english/intro/organization.data:317
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:319
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:321
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:323
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:326
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:329
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:332
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:335
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:342
msgid "User support"
msgstr "Поддръжка за потребители"

#: ../../english/intro/organization.data:409
msgid "Bug Tracking System"
msgstr "Система за следене на грешки"

#: ../../english/intro/organization.data:414
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Администрация и архив на пощенските списъци"

#: ../../english/intro/organization.data:422
msgid "New Members Front Desk"
msgstr "Портал за кандидат-членове"

#: ../../english/intro/organization.data:428
msgid "Debian Account Managers"
msgstr "Администратори на членството в проекта"

#: ../../english/intro/organization.data:432
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:433
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Отговорници за ключодържателите (PGP и GPG)"

#: ../../english/intro/organization.data:437
msgid "Security Team"
msgstr "Екип по сигурността"

#: ../../english/intro/organization.data:448
msgid "Consultants Page"
msgstr "Страница на консултантите"

#: ../../english/intro/organization.data:453
msgid "CD Vendors Page"
msgstr "Страница на търговците на компактдискове"

#: ../../english/intro/organization.data:456
msgid "Policy"
msgstr "Правилник"

#: ../../english/intro/organization.data:459
msgid "System Administration"
msgstr "Системна администрация"

#: ../../english/intro/organization.data:460
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Това е адресът, на който да съобщите при проблеми с някоя от машините на "
"Дебиан, включително проблеми с паролата или липса на пакети."

#: ../../english/intro/organization.data:469
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ако имате хардуерни проблеми с някоя от машините на Дебиан, прочетете "
"страницата <a href=\"https://db.debian.org/machines.cgi\">Машини на Дебиан</"
"a>, която може да съдържа информация за администратора на конкретната машина."

#: ../../english/intro/organization.data:470
msgid "LDAP Developer Directory Administrator"
msgstr "Администратор на директорията на разработчиците в LDAP"

#: ../../english/intro/organization.data:471
msgid "Mirrors"
msgstr "Огледални сървъри"

#: ../../english/intro/organization.data:478
msgid "DNS Maintainer"
msgstr "Отговорник за DNS"

#: ../../english/intro/organization.data:479
msgid "Package Tracking System"
msgstr "Система за следене на пакетите"

#: ../../english/intro/organization.data:481
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:488
#, fuzzy
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"запитвания за използването на <a href=\"m4_HOME/trademark\">търговските "
"марки</a>"

#: ../../english/intro/organization.data:491
#, fuzzy
msgid "Salsa administrators"
msgstr "Администратори на Alioth"

#~ msgid "Debian Pure Blends"
#~ msgstr "Дестилати на Дебиан"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Дебиан за деца от 1 до 99г."

#~ msgid "Debian for medical practice and research"
#~ msgstr "Дебиан за медицински практики и разработки"

#~ msgid "Debian for education"
#~ msgstr "Дебиан за образователни цели"

#~ msgid "Debian in legal offices"
#~ msgstr "Дебиан за правни кантори"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Дебиан за хора с увреждания"

#~ msgid "Debian for science and related research"
#~ msgstr "Дебиан за научни изследвания"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Дебиан за образователни цели"

#~ msgid "Alioth administrators"
#~ msgstr "Администратори на Alioth"

#~ msgid "current Debian Project Leader"
#~ msgstr "текущ лидер на проекта"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Отговорници за програмата „Програмиране през лятото 2013“"

#~ msgid "Alpha (Not active: was not released with squeeze)"
#~ msgstr "Alpha (Неактивна, не е част от изданието squeeze)"

#~ msgid "Handhelds"
#~ msgstr "Джобни компютри"

#~ msgid "Marketing Team"
#~ msgstr "Маркетингов екип"

#~ msgid "Key Signing Coordination"
#~ msgstr "Координация на подписване на ключовете"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Специализирани дистрибуции"

#~ msgid "Release Managers for ``stable''"
#~ msgstr "Управители на стабилните идания"

#~ msgid "Release Assistants"
#~ msgstr "Асистенти"

#~ msgid "APT Team"
#~ msgstr "Екип APT"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Администраторите отговорни за buildd на дадена архитектура могат да бъдат "
#~ "открити на <genericemail архитекрура@buildd.debian.org>, например "
#~ "<genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Имената на отделните администратори на buildd могат да бъдат открити на "
#~ "<a href=\"http://www.buildd.net\">http://www.buildd.net</a>. Изберете "
#~ "артхитектура и дистрибуция за да видите наличните buildd и техните "
#~ "администратори."

#~ msgid "Accountant"
#~ msgstr "Счетоводител"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Универсалната операционна система"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Дебиан за организации с идеална цел"

#~ msgid "Vendors"
#~ msgstr "Търговци"

#~ msgid "Volatile Team"
#~ msgstr "Екип на volatile"

#~ msgid "Security Audit Project"
#~ msgstr "Проект за одит на сигурността"

#~ msgid "Testing Security Team"
#~ msgstr "Екип по сигурността на тестовата дистрибуция"

#~ msgid "DebConf chairs"
#~ msgstr "Ръководство на ДебКонф"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Отговорници за ключодържателите (DM)"

#~ msgid "Bits from Debian"
#~ msgstr "Кратки новини за Дебиан"

#~ msgid "Publicity"
#~ msgstr "Публичност"

#~ msgid "Auditor"
#~ msgstr "Одитор"

#~ msgid "Live System Team"
#~ msgstr "Екип на живата система"

#~ msgid "Individual Packages"
#~ msgstr "Отделни пакети"
