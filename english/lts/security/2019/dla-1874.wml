<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>* <a href="https://security-tracker.debian.org/tracker/CVE-2019-10208">CVE-2019-10208</a>: `TYPE` in `pg_temp` executes arbitrary SQL during
`SECURITY DEFINER` execution</p>

<p>Versions Affected: 9.4 - 11</p>

<p>Given a suitable `SECURITY DEFINER` function, an attacker can execute
arbitrary SQL under the identity of the function owner.  An attack
requires `EXECUTE` permission on the function, which must itself contain
a function call having inexact argument type match.  For example,
`length('foo'::varchar)` and `length('foo')` are inexact, while
`length('foo'::text)` is exact.  As part of exploiting this
vulnerability, the attacker uses `CREATE DOMAIN` to create a type in a
`pg_temp` schema. The attack pattern and fix are similar to that for
<a href="https://security-tracker.debian.org/tracker/CVE-2007-2138">CVE-2007-2138</a>.</p>

<p>Writing `SECURITY DEFINER` functions continues to require following the
considerations noted in the documentation:</p>

<p>https://www.postgresql.org/docs/devel/sql-createfunction.html#SQL-CREATEFUNCTION-SECURITY</p>

<p>The PostgreSQL project thanks Tom Lane for reporting this problem.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
9.4.24-0+deb8u1.</p>

<p>We recommend that you upgrade your postgresql-9.4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1874.data"
# $Id: $
