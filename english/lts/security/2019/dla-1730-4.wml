<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several more boundary checks have been backported to libssh2's
src/sftp.c. Furthermore, all boundary checks in src/sftp.c now result in
an LIBSSH2_ERROR_BUFFER_TOO_SMALL error code, rather than a
LIBSSH2_ERROR_ OUT_OF_BOUNDARY error code.</p>

<p>As a side note, it was discovered that libssh2's SFTP implementation from
Debian jessie only works well against OpenSSH SFTP servers from Debian
wheezy, tests against newer OpenSSH versions (such as available in Debian
jessie and beyond) interim-fail with SFTP protocol error <q>Error opening
remote file</q>. Operation might continue after this error, this depends on
application implementations.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.4.3-4.1+deb8u5.</p>

<p>We recommend that you upgrade your libssh2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1730-4.data"
# $Id: $
