<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was found in the Mercurial version control system
which could lead to remote arbitrary code execution.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17458">CVE-2017-17458</a>

    <p>A specially malformed Mercurial repository could cause Git
    subrepositories to run arbitrary code in the form of a
    .git/hooks/post-update script checked into the parent repository.
    Typical use of Mercurial prevents construction of such repositories,
    but they can be created programmatically.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.2.2-4+deb7u6.</p>

<p>We recommend that you upgrade your mercurial packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1224.data"
# $Id: $
