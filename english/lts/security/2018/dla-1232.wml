<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5754">CVE-2017-5754</a>

    <p>Multiple researchers have discovered a vulnerability in Intel
    processors, enabling an attacker controlling an unprivileged
    process to read memory from arbitrary addresses, including from
    the kernel and all other processes running on the system.</p>

    <p>This specific attack has been named Meltdown and is addressed in
    the Linux kernel for the Intel x86-64 architecture by a patch set
    named Kernel Page Table Isolation, enforcing a near complete
    separation of the kernel and userspace address maps and preventing
    the attack. This solution might have a performance impact, and can
    be disabled at boot time by passing `pti=off' to the kernel
    command line.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17558">CVE-2017-17558</a>

    <p>Andrey Konovalov reported that that USB core did not correctly
    handle some error conditions during initialisation.  A physically
    present user with a specially designed USB device can use this to
    cause a denial of service (crash or memory corruption), or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17741">CVE-2017-17741</a>

    <p>Dmitry Vyukov reported that the KVM implementation for x86 would
    over-read data from memory when emulating an MMIO write if the
    kvm_mmio tracepoint was enabled.  A guest virtual machine might be
    able to use this to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17805">CVE-2017-17805</a>

    <p>It was discovered that some implementations of the Salsa20 block
    cipher did not correctly handle zero-length input.  A local user
    could use this to cause a denial of service (crash) or possibly
    have other security impact.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17806">CVE-2017-17806</a>

    <p>It was discovered that the HMAC implementation could be used with
    an underlying hash algorithm that requires a key, which was not
    intended.  A local user could use this to cause a denial of
    service (crash or memory corruption), or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17807">CVE-2017-17807</a>

    <p>Eric Biggers discovered that the KEYS subsystem lacked a check for
    write permission when adding keys to a process's default keyring.
    A local user could use this to cause a denial of service or to
    obtain sensitive information.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.2.96-3.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1232.data"
# $Id: $
